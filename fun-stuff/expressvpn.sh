#!/bin/bash
# Written by Palanthis
set -e
 
sudo cp /usr/lib/expressvpn/expressvpn.service /etc/systemd/system/

sudo systemctl enable expressvpn

sudo systemctl start expressvpn
